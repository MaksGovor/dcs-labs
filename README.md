# dcs labs

# Говоруха Максим ІП-93

- [Перша версія лаб. роботи (містить файл код та результатів)](https://gitlab.com/MaksGovor/dcs-labs/-/tree/main/Lab1/Version_1)
- [Друга версія лаб. роботи (містить файл код та результатів)](https://gitlab.com/MaksGovor/dcs-labs/-/tree/main/Lab1/Version_2)
- Файл `data.json` є занадто великим і не може бути відобдраженим у web file preview GitLab, тому щоб його переглянути необхідно завантажити репозиторій і переглянути локально
