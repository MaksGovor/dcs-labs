import psycopg2
from dotenv import dotenv_values
config = dotenv_values('.env')

conn = psycopg2.connect(
   database=config['POSTGRES_DB'],
   user=config['POSTGRES_USER'], 
   password=config['POSTGRES_PASSWORD'], 
   host=config['POSTGRES_HOST'], 
   port=config['POSTGRES_PORT']
)

cursor = conn.cursor()
cursor.execute('''CREATE TABLE IF NOT EXISTS accounts
               (id SERIAL PRIMARY KEY, name VARCHAR(55), balance INTEGER, variant VARCHAR(3));''')

cursor.execute('''INSERT INTO accounts (name, balance, variant) VALUES
               ('Alice', 100000, 'htm'), ('Max', 25000, 'htm'), 
               ('John', 50000, 'htm'), ('George', 10, 'htm'), 
               ('Oleg', 100000, 'stm'), ('Lisa', 25000, 'stm'), 
               ('Roman', 50000, 'stm'), ('Anna', 10, 'stm');''')

conn.commit()
conn.close()
