# dcs labs

# Говоруха Максим ІП-93

- [Код програми другої лаб. роботи)](https://gitlab.com/MaksGovor/dcs-labs/-/blob/main/Lab2/PSS_lab2.ipynb)
- [Файл з результатами другої лаб. роботи)](https://gitlab.com/MaksGovor/dcs-labs/-/blob/main/Lab2/result_lab2.txt)
- [Вхідні дані взято з першої лаб. роботи](https://gitlab.com/MaksGovor/dcs-labs/-/blob/main/Lab1/data.json)
- Файл `data.json` є занадто великим і не може бути відобдраженим у web file preview GitLab, тому щоб його переглянути необхідно завантажити репозиторій і переглянути локально
