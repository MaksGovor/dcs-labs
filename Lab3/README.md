# dcs labs

# Говоруха Максим ІП-93

- [Змінні середовища](https://gitlab.com/MaksGovor/dcs-labs/-/blob/main/Lab3/.env)
- [Docker-compose конфіг](https://gitlab.com/MaksGovor/dcs-labs/-/blob/main/Lab3/docker-compose.yml)
- [Скрипт ініціалізації бази даних](https://gitlab.com/MaksGovor/dcs-labs/-/blob/main/Lab3/pss_lab_init.py)
- [Тестова програма для роботи з СКБД (hardware transactional memory)](https://gitlab.com/MaksGovor/dcs-labs/-/blob/main/Lab3/PSS_lab3_htm.ipynb)
- [Тестова програма для роботи з СКБД (software transactional memory)](https://gitlab.com/MaksGovor/dcs-labs/-/blob/main/Lab3/PSS_lab3_stm.ipynb)
